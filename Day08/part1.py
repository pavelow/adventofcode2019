#! /usr/bin/python

data = []

width = 25
height = 6

with open("input.txt") as f:
    line = f.read().rstrip()
    data = [line[i:i+width] for i in range(0, len(line), width)]
    data = ["".join(data[i:i+height]) for i in range(0, len(data), height)]

nZeros = {}
for i in range(len(data)):
    l = data[i]
    nZeros[i] = (l.count('0'))

LayerZ = (min(nZeros, key=nZeros.get))
print(data[LayerZ].count('1') * data[LayerZ].count('2'))