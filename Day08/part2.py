#! /usr/bin/python

data = []

width = 25
height = 6

with open("input.txt") as f:
    line = f.read().rstrip()
    data = [line[i:i+width] for i in range(0, len(line), width)]
    data = [data[i:i+height] for i in range(0, len(data), height)]

image = [[-1 for i in range(width)] for j in range(height)]

for i in range(len(data)):
    layer = data[i]

    for x in range(width):
        for y in range(height):
            if(layer[y][x] == '1' and image[y][x] == -1):
                image[y][x] = 1
            elif(layer[y][x] == '0' and image[y][x] == -1):
                image[y][x] = 0

for y in range(height):
    for x in range(width):
        if(image[y][x] == 0):
            print(' ', end='')
        else:
            print("█", end='')
    print()