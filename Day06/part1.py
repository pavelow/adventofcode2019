#! /usr/bin/python

Data = {}
Bodies = []

with open("input.txt") as f:
   for line in f:
       l = line.rstrip().split(")")
       Data[l[1]] = l[0]
       if(l[1] not in Bodies):
           Bodies.append(l[1])


def Count(Body):
    Orbits = 0
    if(Body in Data):
        if(Data[Body]):
            Orbits += 1
        Orbits += Count(Data[Body])
    return Orbits

Orbits = 0
for Body in Bodies:
    Orbits += Count(Body)

print(Orbits)