#! /usr/bin/python

Data = {}
Bodies = []

with open("input.txt") as f:
   for line in f:
       l = line.rstrip().split(")")
       Data[l[1]] = l[0]
       if(l[1] not in Bodies):
           Bodies.append(l[1])

SAN = {}
YOU = {}

def Count(Body, Dict):
    Orbits = 0
    if(Body in Data):
        if(Data[Body]):
            Orbits += 1
        Orbits += Count(Data[Body], Dict)
        Dict[Data[Body]] = Orbits

    return Orbits

Count("YOU", YOU)
Count("SAN", SAN)

def FindTransfers(Dict, Name):
    sub = Dict[Data[Name]]
    for o in Dict.keys():
        Dict[o] = abs(Dict[o] - sub)
    return Dict

YOU_Transfers = FindTransfers(YOU, "YOU")
SAN_Transfers = FindTransfers(SAN, "SAN")

Common = list(set(SAN_Transfers.keys()).intersection(set(YOU_Transfers.keys())))

print(min(list(map(lambda x: SAN_Transfers[x] + YOU_Transfers[x] , Common))))