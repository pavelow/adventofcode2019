#! /usr/bin/python

import copy, itertools

program = []

with open("input.txt") as f:
    program = f.read().split(",")

program = list(map(int, program))

class Process():
    def __init__(self, program):
        self.program = program
        self.pc = 0

    def Run(self, args):
        argidx = 0
        while True:
            #extract Opcode and Mode
            opcode = int(str(self.program[self.pc])[-2:])
            mode = "000"
            if(len(str(self.program[self.pc])) > 2): #If mode is specified
                mode = str(self.program[self.pc])[:-2]
                mode = mode[::-1]
                if(len(mode) == 2):
                    mode += "0"
                elif(len(mode) == 1):
                    mode += "00"
                

            def Parameter(n):
                if mode[n-1] == "0":
                    return int(self.program[self.program[self.pc+n]]) #Position mode
                else:
                    return int(self.program[self.pc+n]) #Immediate mode       
            
            if(opcode == 1): #Add
                self.program[self.program[self.pc+3]] = Parameter(1) + Parameter(2)
                self.pc += 4
            elif(opcode == 2): #Mul
                self.program[self.program[self.pc+3]] = Parameter(1) * Parameter(2)
                self.pc += 4
            elif(opcode == 3): #Input
                self.program[self.program[self.pc+1]] = args[argidx]#int(input())
                argidx += 1
                self.pc += 2
            elif(opcode == 4): #Print
                out = Parameter(1) 
                self.pc += 2
                return out 
                #print(Parameter(1))
                


            elif(opcode == 5): #Jump if true
                if(Parameter(1) != 0):
                    self.pc = Parameter(2)
                else:
                    self.pc += 3

            elif(opcode == 6): #Jump if false
                if(Parameter(1) == 0):
                    self.pc = Parameter(2)
                else:
                    self.pc += 3

            elif(opcode == 7): #Less than
                if(Parameter(1) < Parameter(2)):
                    self.program[self.program[self.pc+3]] = 1
                else:
                    self.program[self.program[self.pc+3]] = 0
                self.pc += 4

            elif(opcode == 8): #Equals
                if(Parameter(1) == Parameter(2)):
                    self.program[self.program[self.pc+3]] = 1
                else:
                    self.program[self.program[self.pc+3]] = 0
                self.pc += 4
            
            elif(opcode == 99):
                return None
                break
            else:
                print("Error")
                break
        return

def CheckPermutation(perm):
    #Create filter programs
    Processes = []
    for i in range(len(phases)):
        Processes.append(Process(copy.deepcopy(program)))

    r = 0
    for p in range(len(perm)):
        r = Processes[p].Run([perm[p], r])

    while True:
        rp = r
        for p in range(len(perm)):
            r = Processes[p].Run([r])

        if r == None:
            r = rp
            break

    return r

phases = [5,6,7,8,9]
permutations = list(itertools.permutations(phases))

results = []
for p in permutations:
    results.append(CheckPermutation(p))

print(max(results))