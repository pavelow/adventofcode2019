# Advent of Code 2019
Advent of code is a yearly advent calendar with new programming problems each day.
This repository contains my solutions to the [AoC 2019 problems](https://adventofcode.com/2019).

My 2018 solutions can be found [here](https://gitlab.com/pavelow/adventofcode2018).
