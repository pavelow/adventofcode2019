#! /usr/bin/python

input = []
candidates = []

with open("input.txt") as f:
    input = f.readline().rstrip().split('-')

def test(n):
    sn = str(n)
    dcount = 0
    for i in range(len(sn) - 1):
        
        # Two adjacent digits same
        if(sn[i] == sn[i+1]):
            dcount += 1

        # left to right digits never decrease
        if not (sn[i+1] >= sn[i]):
            dcount = 0
            break

    if dcount > 0:
        return True


for n in range(int(input[0]), int(input[1])):
    if(test(n)):
        candidates.append(n)
    
print(len(candidates))