#! /usr/bin/python

input = []
candidates = []

with open("input.txt") as f:
    input = f.readline().rstrip().split('-')

def test(n):
    sn = str(n)
    dcount = 0
    i = 0
    char = sn[0]
    c = 0
    while True:
        # Two or groups of two adjacent digits same
        if i == (len(sn)):
            if(c == 2):
                dcount +=1
            break

        if(sn[i] == char):
            c +=1
        elif(c == 2):
            dcount +=1
            char = sn[i]
            c = 1
        else:
            char = sn[i]
            c = 1
        i += 1
        
   
    for i in range(len(sn) - 1):        
        # left to right digits never decrease
        if not (sn[i+1] >= sn[i]):
            dcount = 0
            break

    if dcount > 0:
        return True
    else:
        return False

if __name__ == "__main__":
    for n in range(int(input[0]), int(input[1])):
        if(test(n)):
            candidates.append(n)

    print(len(candidates))