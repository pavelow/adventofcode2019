#! /usr/bin/python

def Traverse(Line):
    Set = [{},set()]
    Pos = [0,0]
    Steps = 0
    for D in Line:
        Dist = int(D[1:])
        if D[0] == 'U':
            for i in range(Dist):
                Pos[1] += 1
                Steps += 1
                Set[1].add(tuple(Pos))
                if(tuple(Pos) not in Set[0]):
                    Set[0][tuple(Pos)] = Steps
        if D[0] == 'D':
            for i in range(Dist):
                Pos[1] -= 1
                Steps += 1
                Set[1].add(tuple(Pos))
                if(tuple(Pos) not in Set[0]):
                    Set[0][tuple(Pos)] = Steps
        if D[0] == 'L':
            for i in range(Dist):
                Pos[0] -= 1
                Steps += 1
                Set[1].add(tuple(Pos))
                if(tuple(Pos) not in Set[0]):
                    Set[0][tuple(Pos)] = Steps
        if D[0] == 'R':
            for i in range(Dist):
                Pos[0] += 1
                Steps += 1
                Set[1].add(tuple(Pos))
                if(tuple(Pos) not in Set[0]):
                    Set[0][tuple(Pos)] = Steps
    return Set



if __name__ == "__main__":
    lineA = []
    lineB = []

    with open("input.txt") as f:
        lineA = f.readline().rstrip().split(',')
        lineB = f.readline().rstrip().split(',')

    lineACoords = Traverse(lineA)
    lineBCoords = Traverse(lineB)

    Intersections = lineACoords[1].intersection(lineBCoords[1])
    dists = []
    for i in Intersections:
        dists.append(lineACoords[0][i] + lineBCoords[0][i])
    print(min(dists))
