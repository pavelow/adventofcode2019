#! /usr/bin/python

def Traverse(Line):
    Set = set()
    Pos = [0,0]
    for D in Line:
        Dist = int(D[1:])
        if D[0] == 'U':
            for i in range(Dist):
                Pos[1] += 1
                Set.add(tuple(Pos))
        if D[0] == 'D':
            for i in range(Dist):
                Pos[1] -= 1
                Set.add(tuple(Pos))
        if D[0] == 'L':
            for i in range(Dist):
                Pos[0] -= 1
                Set.add(tuple(Pos))
        if D[0] == 'R':
            for i in range(Dist):
                Pos[0] += 1
                Set.add(tuple(Pos))
    return Set



if __name__ == "__main__":
    lineA = []
    lineB = []

    with open("input.txt") as f:
        lineA = f.readline().rstrip().split(',')
        lineB = f.readline().rstrip().split(',')

    lineACoords = Traverse(lineA)
    lineBCoords = Traverse(lineB)

    Intersections = lineACoords.intersection(lineBCoords)
    dists = []
    for i in Intersections:
        dists.append(abs(i[0]) + abs(i[1]))
    print(min(dists))
