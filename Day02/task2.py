#! /usr/bin/python

import copy

program = []
output = 19690720

with open("input.txt") as f:
    program = f.read().split(",")

program = list(map(int, program))




def Run(program):
    pc = 0 #Program counter
    while True:
        if(program[pc] == 1):
            program[program[pc+3]] = program[program[pc+1]] + program[program[pc+2]]
        elif(program[pc] == 2):
            program[program[pc+3]] = program[program[pc+1]] * program[program[pc+2]]
        elif(program[pc] == 99):
            break
        else:
            print("Error")
            break
        pc += 4
    return program


for i in range(0, 99):
    for j in range(0, 99):
        p = copy.deepcopy(program)
        p[1] = i
        p[2] = j
        p = Run(p)
        if(p[0] == output):
            print(p)
            print(100* p[1] + p[2])
            break