#! /usr/bin/python

program = []

with open("input.txt") as f:
    program = f.read().split(",")

program = list(map(int, program))

#Replace numbers
program[1] = 12
program[2] = 2


pc = 0 #Program counter

while True:
    if(program[pc] == 1):
        program[program[pc+3]] = program[program[pc+1]] + program[program[pc+2]]
    elif(program[pc] == 2):
        program[program[pc+3]] = program[program[pc+1]] * program[program[pc+2]]
    elif(program[pc] == 99):
        break
    else:
        print("Error")
        break
    pc += 4

print(program)
