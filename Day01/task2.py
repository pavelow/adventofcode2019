#! /usr/bin/python

from math import floor
sum = 0


def fuel(mass):
    mass = int(mass)
    f = floor(mass/3)-2

    if(f < 0):
        return 0
    else:
        return f + fuel(f)

with open("input.txt") as f:
    for line in f:
        sum += fuel(line)

print(sum)
