#! /usr/bin/python

from math import floor
sum = 0


def fuel(mass):
    return floor(int(mass)/3)-2

with open("input.txt") as f:
    for line in f:
        sum += fuel(line)

print(sum)


